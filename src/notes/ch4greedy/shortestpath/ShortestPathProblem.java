package notes.ch4greedy.shortestpath;

import notes.ch3graphs.CSEVertex;
import notes.ch3graphs.Graph;

/**
 * Created by jessehartloff on 5/11/15.
 */
public class ShortestPathProblem {

    private Graph graph;
    private CSEVertex startingNode;

    private static final double DEFAULT_DENSITY = 0.4;
    private static final int MIN_WEIGHT = 1;
    private static final int MAX_WEIGHT = 200;

    public ShortestPathProblem(int n){
        this(n, DEFAULT_DENSITY);
    }

    public ShortestPathProblem(int n, double density){
        graph = Graph.makeWeightedDirectedGraph(n, density, MIN_WEIGHT, MAX_WEIGHT);
        startingNode = graph.getANode();
    }

    public ShortestPathProblem(Graph graph, CSEVertex startNode){
        this.graph = graph;
        this.startingNode = startNode;
    }

    public Graph getGraph(){
        return graph;
    }


    public CSEVertex getStartingNode(){
        return startingNode;
    }

}
