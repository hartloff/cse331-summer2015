package notes.ch4greedy.shortestpath;

import notes.ch3graphs.CSEEdge;
import notes.ch3graphs.Graph;

import java.util.*;

/**
 * Created by jessehartloff on 6/24/15.
 *
 * Thanks to Keith Schwarz (htiek@cs.stanford.edu) for the implementation of FibonacciHeap.
 *
 */
public class DijkstrasAlgorithm {

    public static int[] computeSolution(ShortestPathProblem shortestPathProblem){


        Graph graph = shortestPathProblem.getGraph();
        Map<Integer, Set<CSEEdge>> outgoingAdjacencyList = graph.getOutgoingAdjacencyList();
        int startNode = shortestPathProblem.getStartingNode().getID();


        Set<Integer> explored = new HashSet<>(graph.getNumberOfNodes());
        int[] distances = new int[graph.getNumberOfNodes()];
        int[] previousNode = new int[graph.getNumberOfNodes()];
        List<FibonacciHeap.Entry> entries = new ArrayList<>(graph.getNumberOfNodes());

        for (int i = 0; i < distances.length; i++) {
            distances[i] = Integer.MAX_VALUE;
            entries.add(null);
            previousNode[i] = -1;
        }

        FibonacciHeap<Integer> testing = new FibonacciHeap<>();

        entries.set(startNode, testing.enqueue(startNode, 0));
        distances[startNode] = 0;


        while(!testing.isEmpty()){
            FibonacciHeap.Entry entry = testing.dequeueMin();

            int currentNode = (int)entry.getValue();

            if(explored.contains(currentNode)){
                continue;
            }
            distances[currentNode] = (int)(entry.getPriority() + 0.1);
            explored.add(currentNode);

            for(CSEEdge edgeToCheck : outgoingAdjacencyList.get(currentNode)){
                if(explored.contains(edgeToCheck.getDestination())){
                    continue;
                }
                int distanceFound = distances[currentNode] + edgeToCheck.getWeight();
                int oldDistance = distances[edgeToCheck.getDestination()];
                if(distanceFound < oldDistance){
                    distances[edgeToCheck.getDestination()] = distanceFound;
                    if(entries.get(edgeToCheck.getDestination()) == null){
                        entries.set(edgeToCheck.getDestination(), testing.enqueue(edgeToCheck.getDestination(), distanceFound));
                    }else {
                        testing.decreaseKey(entries.get(edgeToCheck.getDestination()), distanceFound);
                    }
                    previousNode[edgeToCheck.getDestination()] = currentNode;
                }
            }
        }

        printPath(previousNode);
        return distances;
    }


    public static void printDistances(int[] distances){
        for (int i = 0; i < distances.length; i++) {
            System.out.println("distance to node " + i + " is " + distances[i]);
        }
    }

    public static void printPath(int[] distances){
        for (int i = 0; i < distances.length; i++) {
            System.out.println(distances[i] + " connects to " + i);
        }
    }


}
