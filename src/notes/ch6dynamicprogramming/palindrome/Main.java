package notes.ch6dynamicprogramming.palindrome;

import java.util.Arrays;

/**
 * Created by jessehartloff on 7/23/15.
 */
public class Main {

    public static void main(String[] args) {
        PalindromeProblem problem = new PalindromeProblem(200);
        System.out.println(problem);

        int[] bestDrome = BruteForcePalindromeFinder.findBestPalindrome(problem);

        System.out.println();
        System.out.println(Arrays.toString(bestDrome));
        System.out.println(problem.solutionValue(bestDrome));
    }

}
