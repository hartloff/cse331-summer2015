package notes.ch6dynamicprogramming.knapsack;

import java.util.Set;

/**
 * Created by jessehartloff on 6/26/15.
 */
public class Main {

    public static void main(String[] args) {

        int n = 100;
        int capacity = 2000;
        KnapsackProblem problem = new KnapsackProblem(n, capacity);

//        List<Item> allItems = new ArrayList<>(problem.getItems());
//        Collections.sort(allItems, new Item.WeightComparator());
//        System.out.println(allItems);

        System.out.println(problem);

        System.out.println();


        Set<Item> greedySolution = GreedyKnapsack.generateSolution(problem);

        System.out.println();
        System.out.println("Greedy Solution:");
        System.out.println(greedySolution);
        System.out.println("weight: " + KnapsackProblem.checkWeight(greedySolution));
        System.out.println("value: " + KnapsackProblem.checkValue(greedySolution));


//        Set<Item> bruteForceSolution = BruteForceKnapsack.generateSolution(problem);
//
//        System.out.println();
//        System.out.println("Brute Force Solution:");
//        System.out.println(bruteForceSolution);
//        System.out.println("weight: " + KnapsackProblem.checkWeight(bruteForceSolution));
//        System.out.println("value: " + KnapsackProblem.checkValue(bruteForceSolution));


        Set<Item> optimalSolution = KnapsackAlgorithm.generateSolution(problem);

        System.out.println();
        System.out.println("Optimal Solution");
        System.out.println(optimalSolution);
        System.out.println("weight: " + KnapsackProblem.checkWeight(optimalSolution));
        System.out.println("value: " + KnapsackProblem.checkValue(optimalSolution));
    }

}
