package assignments.assignment3;

/**
 * Created by jessehartloff on 6/30/15.
 */
public class Main {

    private final static int MAX_N = 20;
    private final static int MAX_K = 4;


    /**
     * Test class to help check the performance of Assignment3.securityScreen. This is meant to assist testing
     * and will be similar, though not identical, to what will be used for grading.
     */
    public static void main(String[] args) {

        for (int k = 1; k <= MAX_K; k++) {
            for (int n = k+1; n <= MAX_N; n++) {
                int worstCaseFakeLaunches = testAll(n - 1, k);
                if(worstCaseFakeLaunches == Integer.MAX_VALUE){
                    System.out.println("launched with attacker(s) or never launched");
                }else {
                    System.out.println("n=" + n + " k=" + k + " successfully launched with " + worstCaseFakeLaunches +
                            " fake launches in the worst case");
                }
            }
        }

    }


    //returns the worst case number of fake launches for a given k and n
    private static int testAll(int n, int k) {
        return testAll(new boolean[n+1], n, k, k);
    }


    private static int testAll(boolean[] attackers, int n, int k, int totalK){

        if(n+1<k || k<0){
            return 0;
        }

        if(n<0){
            Website site = new Website(attackers);
            Assignment3.securityScreen(site, totalK);
            if(site.successfulLaunch()){
                return site.getNumberOfFakeLaunches();
            }else{
                return Integer.MAX_VALUE;
            }
        }

        attackers[n] = true;
        int launchesWith = testAll(attackers, n - 1, k - 1, totalK);

        attackers[n] = false;
        int launchesWithout = testAll(attackers, n - 1, k, totalK);

        return Math.max(launchesWith, launchesWithout);
    }

}
