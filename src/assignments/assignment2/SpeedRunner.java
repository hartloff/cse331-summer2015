package assignments.assignment2;

import java.util.Random;

/**
 * Created by jessehartloff on 6/22/15.
 */
public class SpeedRunner {

    private int marioTime;
    private int doomTime;

    private int minMarioTime = 290;
    private int maxMarioTime = 1000;

    private int minDoomTime = 1380;
    private int maxDoomTime = 5000;


    public SpeedRunner(){
        generateRandom();
    }

    public SpeedRunner(int marioTime, int doomTime){
        this.marioTime = marioTime;
        this.doomTime = doomTime;
    }

    private void generateRandom(){
        Random random = new Random();
        marioTime = random.nextInt(maxMarioTime - minMarioTime) + minMarioTime;
        doomTime = random.nextInt(maxDoomTime - minDoomTime) + minDoomTime;
    }

    public int getMarioTime(){
        return marioTime;
    }

    public int getDoomTime(){
        return doomTime;
    }

    @Override
    public String toString() {
        return "{" +
                "marioTime=" + marioTime +
                ", doomTime=" + doomTime +
                '}';
    }
}
