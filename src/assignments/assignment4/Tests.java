package assignments.assignment4;

import junit.framework.TestCase;
import org.junit.Test;

import java.util.Arrays;

/**
 * Created by jessehartloff on 7/20/15.
 */
public class Tests {

    // The purpose of these tests to ensure that we are using the same conventions when computing the cost of a
    // schedule. Specifically, the schedule is true at i if the elevator is to be risen to the top using the alternate
    // fuel after using dark matter i.
    //
    // Additionally, except for the invalid schedule test, these examples give the optimal schedule for the input. You
    // are encouraged to write your own tests using this data to check if your algorithm is optimal.

    @Test
    public void computeCostInvalidTest(){
        Integer[] darkMatter = new Integer[] {17, 6, 18, 19, 13, 11, 14, 6, 16, 10, 5, 15, 12, 9, 6, 19, 14, 10, 14, 10};
        boolean[] invalidSchedule = new boolean[] {false, false, false, false, true, false, false, true, false, false, true, true,
                true, false, true, true, false, true, false, true};

        SpaceElevator elevator = new SpaceElevator(Arrays.asList(darkMatter), new Oil());

        double computed = Assignment4.computeCost(elevator, invalidSchedule);
        double expected = Double.POSITIVE_INFINITY;

        TestCase.assertTrue("\nExpected: " + expected + "\nComputed: " + computed, expected == computed || Math.abs(computed - expected) < 0.01);
    }

    @Test
    public void computeCostTest0(){
        Integer[] darkMatter = new Integer[] {15, 14, 5, 9, 12, 16, 5, 4, 9, 8, 12, 7, 10, 15, 18, 15, 16, 17, 6, 17};
        boolean[] schedule = new boolean[] {true, false, true, false, true, false, true, false, false, true, false,
                true, true, true, true, true, true, false, true, false};

        SpaceElevator elevator = new SpaceElevator(Arrays.asList(darkMatter), new NuclearFuel());

        double computed = Assignment4.computeCost(elevator, schedule);
        double expected = 6300.0;

        TestCase.assertTrue("\nExpected: " + expected + "\nComputed: " + computed, expected == computed || Math.abs(computed - expected) < 0.01);
    }

    @Test
    public void computeCostTest1(){
        Integer[] darkMatter = new Integer[] {4, 9, 6, 13, 5, 11, 7, 3, 7, 7, 12, 8, 7, 17, 15, 6, 10, 4, 9, 11};
        boolean[] schedule = new boolean[] {false, true, false, true, false, true, false, false, true, false, true,
                false, true, true, false, true, false, false, true, false};

        SpaceElevator elevator = new SpaceElevator(Arrays.asList(darkMatter), new Oil());

        double computed = Assignment4.computeCost(elevator, schedule);
        double expected = 1284.0;

        TestCase.assertTrue("\nExpected: " + expected + "\nComputed: " + computed, expected == computed || Math.abs(computed - expected) < 0.01);
    }

    @Test
    public void computeCostTest2(){
        Integer[] darkMatter = new Integer[] {17, 6, 18, 19, 13, 11, 14, 6, 16, 10, 5, 15, 12, 9, 6, 19, 14, 10, 14, 10};
        boolean[] schedule = new boolean[] {false, true, true, true, true, true, false, true, true, false, true, true,
                true, false, true, true, true, true, true, false};

        SpaceElevator elevator = new SpaceElevator(Arrays.asList(darkMatter), new SteamPower());

        double computed = Assignment4.computeCost(elevator, schedule);
        double expected = 493650.0;

        TestCase.assertTrue("\nExpected: " + expected + "\nComputed: " + computed, expected == computed || Math.abs(computed - expected) < 0.01);
    }

}
