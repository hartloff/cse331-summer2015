package assignments.assignment4;

/**
 * Created by jessehartloff on 7/19/15.
 */
public class Oil implements AlternateFuel {

    @Override
    public double cost(int remainingDistance) {
        return 4.0*Math.pow(remainingDistance, 2.0);
    }

    @Override
    public int getID() {
        return AlternateFuel.OIL;
    }

    @Override
    public String toString() {
        return "Oil";
    }

}
