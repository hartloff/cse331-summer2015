package assignments.assignment4;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by jessehartloff on 7/15/15.
 */
public class SpaceElevator {

    private AlternateFuel alternateFuel;
    private List<Integer> darkMatter;

    private static final int DARK_MATTER_MIN_ENERGY = 3;
    private static final int DARK_MATTER_MAX_ENERGY = 20;
    private static final int ELEVATOR_HEIGHT = 23; // in 1000's of miles


    /**
     * Creates an elevator with a given alternate fuel and n random upcoming dark matter balls
     */
    public SpaceElevator(int n, AlternateFuel alternateFuel){
        this.alternateFuel = alternateFuel;
        darkMatter = new ArrayList<>(n);
        Random random = new Random();
        for (int i = 0; i < n; i++) {
            darkMatter.add(random.nextInt(DARK_MATTER_MAX_ENERGY - DARK_MATTER_MIN_ENERGY)
                    + DARK_MATTER_MIN_ENERGY);
        }
    }

    /**
     * Creates an elevator with a given alternate fuel and upcoming dark matter balls
     */
    public SpaceElevator(List<Integer> darkMatter, AlternateFuel alternateFuel){
        this.alternateFuel = alternateFuel;
        this.darkMatter = new ArrayList<>(darkMatter);
    }

    public List<Integer> getUpcomingDarkMatterDrops(){
        return new ArrayList<>(darkMatter);
    }

    public double getAlternateFuelCost(int remainingHeight){
        return alternateFuel.cost(remainingHeight);
    }

    public int getAlternateFuelID(){
        return alternateFuel.getID();
    }

    public int getElevatorHeight(){
        return ELEVATOR_HEIGHT;
    }

    @Override
    public String toString() {
        return alternateFuel + ", upcoming dark matter: " + darkMatter;
    }
}
