package assignments.assignment4;

/**
 * Created by jessehartloff on 7/19/15.
 */
public class NuclearFuel implements AlternateFuel {

    @Override
    public double cost(int remainingDistance) {
        return 100.0*remainingDistance;
    }

    @Override
    public int getID() {
        return AlternateFuel.NUCLEAR;
    }

    @Override
    public String toString() {
        return "Nuclear Fuel";
    }
}
